//! These are tests concerning the API of the employees service. See `tx_logic.rs`
//! for tests focused on the business logic of transactions.

extern crate exonum;
extern crate exonum_employees as employees;
extern crate exonum_testkit;
#[macro_use]
extern crate serde_json;

use exonum::crypto::{PublicKey, SecretKey, CryptoHash, Hash};
use exonum::encoding::serialize::FromHex;
use exonum_testkit::{ApiKind, TestKit, TestKitApi, TestKitBuilder};

// Import data types used in tests from the crate where the service is defined.
use employees::transactions::{TxCreateEmployee, TxUpdateEmployee};
use employees::schema::Employee;
use employees::service::EmployeesService;

// Imports shared test constants.
use constants::*;

mod constants;

/// Convenient wrapper for service api
struct EmployeesApi {
    pub inner: TestKitApi,
}

impl EmployeesApi {
    fn create_employee(
        &self,
        id: u64,
        first_name: &str,
        last_name: &str,
        pub_key: &PublicKey,
        info: &str,
        key: &SecretKey,
    )
        -> TxCreateEmployee
    {
        let tx = TxCreateEmployee::new(id, first_name, last_name, pub_key, info, &key);

        let tx_info: serde_json::Value = self.inner.post(
            ApiKind::Service("employees"),
            "v1/create",
            &tx,
        );

        assert_eq!(tx_info, json!({ "tx_hash": tx.hash() }));
        tx
    }

    fn update_employee(
        &self,
        target_pub_key: &PublicKey,
        first_name: &str,
        last_name: &str,
        pub_key: &PublicKey,
        info: &str,
        key: &SecretKey,
    )
        -> TxUpdateEmployee
    {
        let tx = TxUpdateEmployee::new(
            target_pub_key,
            first_name,
            last_name,
            pub_key,
            info,
            key,
        );

        let tx_info: serde_json::Value = self.inner.post(
            ApiKind::Service("employees"),
            "v1/update",
            &tx,
        );

        assert_eq!(tx_info, json!({ "tx_hash": tx.hash() }));
        tx
    }

    /// Asserts that the transaction with the given hash has a specified status.
    fn assert_tx_status(&self, tx_hash: &Hash, expected_status: &serde_json::Value) {
        let info: serde_json::Value = self.inner.get(
            ApiKind::Explorer,
            &format!("v1/transactions/{}", tx_hash.to_string()),
        );
        if let serde_json::Value::Object(mut info) = info {
            let tx_status = info.remove("status").unwrap();
            assert_eq!(tx_status, *expected_status);
        } else {
            panic!("Invalid transaction info format, object expected");
        }
    }

    /// Returns block heights, associated with employee's id
    fn get_blocks(&self, id: u64) -> Vec<u64> {
        self.inner.get(
            ApiKind::Service("employees"),
            &format!("v1/blocks/{}", id),
        )
    }

    /// Returns employee by `pub_key`.
    fn get_employee(&self, pub_key: &PublicKey) -> Employee {
        self.inner.get(
            ApiKind::Service("employees"),
            &format!("v1/keys/{}", pub_key.to_string()),
        )
    }

    /// Returns error, emitted while getting employee by `pub_key`.
    fn get_employee_err(&self, pub_key: &PublicKey) -> serde_json::Value {
        self.inner.get_err(
            ApiKind::Service("employees"),
            &format!("v1/keys/{}", pub_key.to_string()),
        )
    }

    /// Returns employee by `id`.
    fn get_employee_by_id(&self, id: u64) -> Employee {
        self.inner.get(
            ApiKind::Service("employees"),
            &format!("v1/ids/{}", id),
        )
    }
}

fn create_testkit() -> (TestKit, EmployeesApi) {
    let testkit = TestKitBuilder::validator()
        .with_service(EmployeesService)
        .create();
    let api = EmployeesApi { inner: testkit.api() };
    (testkit, api)
}

#[test]
fn superuser_should_be_able_to_create_employee() {
    let (mut test_kit, api) = create_testkit();

    let alice_pub_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_priv_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_pub_key, "foo", &su_priv_key);
    test_kit.create_block();

    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Check that employee saved
    let employee = api.get_employee(tx.pub_key());
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_pub_key);
    assert_eq!(employee.info(), "foo");

    // Check id index
    assert_eq!(employee, api.get_employee_by_id(1));
}

#[test]
fn superuser_should_be_able_to_update_employee() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let carol_key = PublicKey::from_hex(CAROL_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Update with superuser's key
    let tx = api.update_employee(&alice_key, "Alice", "Pixton", &carol_key, "bar", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Check that employee is updated
    let employee = api.get_employee(tx.pub_key());
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Pixton");
    assert_eq!(employee.pub_key(), &carol_key);
    assert_eq!(employee.info(), "bar");
}

#[test]
fn employee_should_be_able_to_update_itself() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let alice_priv = SecretKey::from_hex(ALICE_PRIV_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Update with employee's key
    let tx = api.update_employee(&alice_key, "Alice_", "Persky_", &alice_key, "bar", &alice_priv);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Check that employee is updated
    let employee = api.get_employee(tx.pub_key());
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice_");
    assert_eq!(employee.last_name(), "Persky_");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "bar");
}

#[test]
fn should_correctly_handle_pub_key_update() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let tx = api.update_employee(&alice_key, "Alice", "Persky", &bob_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Employees pub_key should be updated
    let employee = api.get_employee(&bob_key);
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &bob_key);
    assert_eq!(employee.info(), "foo");

    // Index should be updated
    let employee = api.get_employee_by_id(1);
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &bob_key);
    assert_eq!(employee.info(), "foo");

    // Old pub_key should be deleted
    let error = api.get_employee_err(&alice_key);
    assert_eq!(error, json!("Employee not found"));
}

#[test]
fn unknown_key_should_not_be_able_to_update_employee() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let bob_priv = SecretKey::from_hex(BOB_PRIV_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let tx = api.update_employee(&alice_key, "Alice", "Persky", &bob_key, "bar", &bob_priv);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({
        "type": "error",
        "description": "Forbidden",
        "code": employees::errors::Error::Forbidden as u8,
    }));
}

#[test]
fn should_reject_create_employee_tx_on_public_key_conflict() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let tx = api.create_employee(2, "Bob", "Maddox", &alice_key, "", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({
        "type": "error",
        "description": "Employee already exists",
        "code": employees::errors::Error::EmployeeAlreadyExists as u8,
    }));
}

#[test]
fn should_reject_create_employee_tx_on_id_conflict() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let tx = api.create_employee(1, "Bob", "Maddox", &bob_key, "", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({
        "type": "error",
        "description": "Employee already exists",
        "code": employees::errors::Error::EmployeeAlreadyExists as u8,
    }));
}

#[test]
fn should_reject_update_employee_tx_on_public_key_conflict() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let tx = api.create_employee(2, "Bob", "Maddox", &bob_key, "", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    let tx = api.update_employee(&alice_key, "Alice", "Persky", &bob_key, "bar", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({
        "type": "error",
        "description": "Employee already exists",
        "code": employees::errors::Error::EmployeeAlreadyExists as u8,
    }));
}

#[test]
#[should_panic]
fn should_reject_tx_with_empty_first_name_on_create() {
    let (_test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_priv_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    api.create_employee(1, "", "Persky", &alice_key, "foo", &su_priv_key);
}

#[test]
#[should_panic]
fn should_reject_tx_with_empty_last_name_on_create() {
    let (_test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_priv_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    api.create_employee(1, "Alice", "", &alice_key, "foo", &su_priv_key);
}

#[test]
#[should_panic]
fn should_reject_tx_with_empty_first_name_on_update() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_priv_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_priv_key);
    test_kit.create_block();
    api.update_employee(&alice_key, "", "Persky", &alice_key, "foo", &su_priv_key);
}

#[test]
#[should_panic]
fn should_reject_tx_with_empty_last_name_on_update() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_priv_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_priv_key);
    test_kit.create_block();
    api.update_employee(&alice_key, "Alice", "", &alice_key, "foo", &su_priv_key);
}

#[test]
fn blocks_should_be_associated_with_employees_id_on_its_create_and_update() {
    let (mut test_kit, api) = create_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx1 = api.create_employee(1, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx1.hash(), &json!({ "type": "success" }));

    // Check that first block is associated with Alice's id
    assert_eq!(vec![1], api.get_blocks(tx1.id()));

    let tx2 = api.create_employee(2, "Bob", "Maddox", &bob_key, "bar", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx2.hash(), &json!({ "type": "success" }));

    // Check that second block is associated with Bob's id
    assert_eq!(vec![2], api.get_blocks(tx2.id()));

    let tx = api.update_employee(&alice_key, "Alice", "Persky", &alice_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Check that now third block is also associated with Alice's id
    assert_eq!(vec![1, 3], api.get_blocks(tx1.id()));

    let tx = api.update_employee(&bob_key, "Bob", "Maddox", &bob_key, "foo", &su_key);
    test_kit.create_block();
    api.assert_tx_status(&tx.hash(), &json!({ "type": "success" }));

    // Check that now forth block is also associated with Bob's id
    assert_eq!(vec![2, 4], api.get_blocks(tx2.id()));
}
