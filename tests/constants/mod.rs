// Copyright 2018 The Exonum Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Shared constants used both in api and transactions logic tests.

pub const SU_SECRET_KEY: &str = "87fb7950f968e5121d13d5797d1faa37a13e77215e1e770af0c7456ca58a225e20c41714c92cfe7200f40b86fae30e6d6ae76aa2bf94526e0e4f54c9fd65efb9";

pub const ALICE_PUB_KEY: &str = "1c714ff478bf86f56447c6c40e67af7940c00f4ca83f9007dab9a4059ecbe235";
pub const ALICE_PRIV_KEY: &str = "4aaa07dc805df1cac1c64d15a711be3c74908b2af83b67e21f4e0ae5cf1240211c714ff478bf86f56447c6c40e67af7940c00f4ca83f9007dab9a4059ecbe235";

pub const BOB_PUB_KEY: &str = "baf611d4929866aef785f1516f883f41116f99d53321416c19c54a067ed9d2f5";
pub const BOB_PRIV_KEY: &str = "9648a4687d20364a5d9125f7ee592ae806cee0d18c995a97fe0975f190110b06baf611d4929866aef785f1516f883f41116f99d53321416c19c54a067ed9d2f5";

#[allow(dead_code)]
pub const CAROL_PUB_KEY: &str = "c1898a132bce02eaf3f1dff62d4d14a1de0fbf67abfa26d915d0d40869794719";
#[allow(dead_code)]
pub const CAROL_PRIV_KEY: &str = "b92e1bf02582d1473a34e92e4f366b0a5eb6cbc479df5dc2251a18c887e6c95bc1898a132bce02eaf3f1dff62d4d14a1de0fbf67abfa26d915d0d40869794719";
