// Copyright 2018 The Exonum Team
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! These are tests concerning the business logic of transactions in the cryptocurrency service.
//! See `api.rs` for tests focused on the API of the service.
//!
//! Note how business logic tests use `TestKit::create_block*` methods to send transactions,
//! the service schema to make assertions about the storage state.

extern crate exonum;
extern crate exonum_employees as employees;
extern crate exonum_testkit;
extern crate rand;

use exonum::crypto::{CryptoHash, Hash, PublicKey, SecretKey};
use exonum::encoding::serialize::FromHex;
use exonum_testkit::{TestKit, TestKitBuilder};

// Import data types used in tests from the crate where the service is defined.
use employees::schema::{EmployeeSchema, Employee};
use employees::transactions::{TxCreateEmployee, TxUpdateEmployee};
use employees::service::EmployeesService;

// Imports shared test constants.
use constants::*;

mod constants;

/// Initialize testkit with `EmployeesService`.
fn init_testkit() -> TestKit {
    TestKitBuilder::validator()
        .with_service(EmployeesService)
        .create()
}

fn create_employee(
    testkit: &mut TestKit,
    id: u64,
    first_name: &str,
    last_name: &str,
    pub_key: &PublicKey,
    info: &str,
    signers_key: &SecretKey,
)
    -> TxCreateEmployee
{
    let tx = TxCreateEmployee::new(id, first_name, last_name, pub_key, info, signers_key);
    testkit.create_block_with_transaction(tx.clone());
    tx
}

fn update_employee(
    testkit: &mut TestKit,
    target_pub_key: &PublicKey,
    first_name: &str,
    last_name: &str,
    pub_key: &PublicKey,
    info: &str,
    signers_key: &SecretKey,
)
    -> TxUpdateEmployee
{
    let tx = TxUpdateEmployee::new(
        target_pub_key,
        first_name,
        last_name,
        pub_key,
        info,
        signers_key
    );

    testkit.create_block_with_transaction(tx.clone());
    tx
}

fn get_employee(test_kit: &TestKit, pub_key: &PublicKey) -> Option<Employee> {
    let snapshot = test_kit.snapshot();
    EmployeeSchema::new(&snapshot).employee(pub_key)
}

fn get_employee_by_id(test_kit: &TestKit, id: u64) -> Option<Employee> {
    let snapshot = test_kit.snapshot();
    EmployeeSchema::new(&snapshot).employee_by_id(id)
}

fn get_employees_txs(test_kit: &TestKit, id: u64) -> Vec<Hash> {
    let snapshot = test_kit.snapshot();
    let mut hashes = Vec::new();
    let schema = EmployeeSchema::new(&snapshot);
    let emps_txs = schema.employees_txs(id);
    // no `collect` here because of borrow checker
    for hash in emps_txs.iter() {
        hashes.push(hash);
    }
    hashes
}

fn get_known_ids(test_kit: &TestKit) -> Vec<u64> {
    let snapshot = test_kit.snapshot();
    let schema = EmployeeSchema::new(snapshot);
    let known_ids = schema.known_ids();
    let out = known_ids.iter().collect::<Vec<u64>>(); // to trick borrow checker
    out
}

#[test]
fn superuser_should_be_able_to_create_employee() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx = create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);

    // Check that employee saved
    let employee = get_employee(&test_kit, tx.pub_key()).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "");

    // Check that index updated
    let employee2 = get_employee_by_id(&test_kit, tx.id()).unwrap();
    assert_eq!(employee, employee2);
}

#[test]
fn should_fill_known_ids_on_create_employee() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    assert_eq!(get_known_ids(&test_kit), Vec::<u64>::new());
    create_employee(&mut test_kit, 3, "Alice", "Persky", &alice_key, "", &su_key);
    assert_eq!(get_known_ids(&test_kit), vec![3]);
    create_employee(&mut test_kit, 1, "Bob", "Maddox", &bob_key, "", &su_key);
    assert_eq!(get_known_ids(&test_kit), vec![3, 1]);
}

#[test]
fn superuser_should_be_able_to_update_employee() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    update_employee(&mut test_kit, &alice_key, "Alice_", "Persky_", &alice_key, "foo", &su_key);

    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice_");
    assert_eq!(employee.last_name(), "Persky_");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "foo");
}

#[test]
fn employee_should_be_able_to_update_itself() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let alice_priv = SecretKey::from_hex(ALICE_PRIV_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    update_employee(&mut test_kit, &alice_key, "Alice_", "Persky_", &alice_key, "foo", &alice_priv);

    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice_");
    assert_eq!(employee.last_name(), "Persky_");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "foo");
}

#[test]
fn should_correctly_handle_pub_key_update() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let alice_priv = SecretKey::from_hex(ALICE_PRIV_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    update_employee(&mut test_kit, &alice_key, "Alice", "Persky", &bob_key, "", &alice_priv);

    // Old key should be deleted
    assert!(get_employee(&test_kit, &alice_key).is_none());

    // New key should be created
    let employee = get_employee(&test_kit, &bob_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &bob_key);
    assert_eq!(employee.info(), "");

    // Id index should be updated
    let employee2 = get_employee_by_id(&test_kit, 1).unwrap();
    assert_eq!(employee, employee2);
}

#[test]
fn unknown_key_should_not_be_able_to_update_employee() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let bob_priv = SecretKey::from_hex(BOB_PRIV_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    update_employee(&mut test_kit, &alice_key, "Alice", "Persky", &bob_key, "x", &bob_priv);

    // Update should fail
    assert!(get_employee(&test_kit, &bob_key).is_none());
    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "");
}

#[test]
fn create_employee_tx_should_fail_on_pub_key_conflict() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    create_employee(&mut test_kit, 2, "Bob", "Maddox", &alice_key, "", &su_key);

    // First create should pass
    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "");

    // Second create should fail
    assert!(get_employee_by_id(&test_kit, 2).is_none());
}

#[test]
fn create_employee_tx_should_fail_on_id_conflict() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    create_employee(&mut test_kit, 1, "Bob", "Maddox", &bob_key, "", &su_key);

    // First create should pass
    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "");

    // Second create should fail
    assert!(get_employee(&test_kit, &bob_key).is_none());
}

#[test]
fn update_employee_tx_should_fail_on_pub_key_conflict() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    create_employee(&mut test_kit, 2, "Bob", "Maddox", &bob_key, "", &su_key);
    update_employee(&mut test_kit, &alice_key, "A", "A", &bob_key, "", &su_key);

    // First create should pass, update should fail
    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "");
    assert_eq!(employee, get_employee_by_id(&test_kit, 1).unwrap());

    // Second create should pass
    let employee = get_employee(&test_kit, &bob_key).unwrap();
    assert_eq!(employee.id(), 2);
    assert_eq!(employee.first_name(), "Bob");
    assert_eq!(employee.last_name(), "Maddox");
    assert_eq!(employee.pub_key(), &bob_key);
    assert_eq!(employee.info(), "");
    assert_eq!(employee, get_employee_by_id(&test_kit, 2).unwrap());
}

#[test]
fn should_reject_txs_with_empty_required_fields() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    create_employee(&mut test_kit, 1, "Alice", "", &alice_key, "", &su_key);
    // Tx should fail, last_name required on create
    assert!(get_employee(&test_kit, &alice_key).is_none());
    assert!(get_employee_by_id(&test_kit, 1).is_none());

    create_employee(&mut test_kit, 1, "", "Persky", &alice_key, "", &su_key);
    // Tx should fail, first_name required on create
    assert!(get_employee(&test_kit, &alice_key).is_none());
    assert!(get_employee_by_id(&test_kit, 1).is_none());

    create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    let employee = get_employee(&test_kit, &alice_key).unwrap();
    assert_eq!(employee.id(), 1);
    assert_eq!(employee.first_name(), "Alice");
    assert_eq!(employee.last_name(), "Persky");
    assert_eq!(employee.pub_key(), &alice_key);
    assert_eq!(employee.info(), "");
    assert_eq!(employee, get_employee_by_id(&test_kit, 1).unwrap());

    update_employee(&mut test_kit, &alice_key, "Alice", "", &alice_key, "", &su_key);
    // Tx should fail, last_name required on create
    assert_eq!(employee, get_employee(&test_kit, &alice_key).unwrap());
    assert_eq!(employee, get_employee_by_id(&test_kit, 1).unwrap());

    update_employee(&mut test_kit, &alice_key, "", "Persky", &alice_key, "", &su_key);
    // Tx should fail, fist_name required on create
    assert_eq!(employee, get_employee(&test_kit, &alice_key).unwrap());
    assert_eq!(employee, get_employee_by_id(&test_kit, 1).unwrap());
}

#[test]
fn tx_hashes_should_be_associated_with_employees_id_on_its_create_and_update() {
    let mut test_kit = init_testkit();

    let alice_key = PublicKey::from_hex(ALICE_PUB_KEY).unwrap();
    let bob_key = PublicKey::from_hex(BOB_PUB_KEY).unwrap();
    let su_key = SecretKey::from_hex(SU_SECRET_KEY).unwrap();

    let tx1 = create_employee(&mut test_kit, 1, "Alice", "Persky", &alice_key, "", &su_key);
    // Check that tx1 is now associated with Alice's id
    assert_eq!(vec![tx1.hash()], get_employees_txs(&test_kit, tx1.id()));

    let tx2 = create_employee(&mut test_kit, 2, "Bob", "Maddox", &bob_key, "", &su_key);
    // Check that tx2 is now associated with Bob's id
    assert_eq!(vec![tx2.hash()], get_employees_txs(&test_kit, tx2.id()));

    let tx3 = update_employee(&mut test_kit, &alice_key, "Alice", "Persky", &alice_key, "", &su_key);
    // Check that tx3 is now also associated with Alice's id
    assert_eq!(vec![tx1.hash(), tx3.hash()], get_employees_txs(&test_kit, tx1.id()));

    let tx4 = update_employee(&mut test_kit, &bob_key, "Bob", "Maddox", &bob_key, "", &su_key);
    // Check that tx4 is now also associated with Bob's id
    assert_eq!(vec![tx2.hash(), tx4.hash()], get_employees_txs(&test_kit, tx2.id()));
}
