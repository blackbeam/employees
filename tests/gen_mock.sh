#!/usr/bin/env bash

paste -d ',' tests/mock_source.csv tests/pub_keys.txt \
    | csvformat -D'№' \
    | awk "{ split(\$0,a,\"№\"); print a[1] \" \" a[2] \" \" a[3] \" '\" a[4] \"' \" a[5] }" \
    | xargs -L 1 cargo run --release -q --example signer \
    | xargs -L 1 > tests/create_sigs.txt

paste -d ',' tests/mock_source.csv tests/mock_source_upd.csv tests/pub_keys.txt \
    | csvformat -D'№' \
    | awk "{ split(\$0,a,\"№\"); print a[8] \" \" a[5] \" \" a[6] \" '\" a[7] \"'\" }" \
    | xargs -L 1 cargo run --release -q --example signer \
    | xargs -L 1 > tests/update_sigs.txt

paste -d ',' \
    tests/mock_source.csv \
    tests/pub_keys.txt \
    tests/create_sigs.txt \
    tests/mock_source_upd.csv \
    tests/update_sigs.txt > tests/mock.csv
