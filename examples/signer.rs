extern crate exonum;
extern crate exonum_employees as employees;

use employees::transactions::{TxCreateEmployee, TxUpdateEmployee};
use exonum::crypto::{PublicKey, SecretKey};
use exonum::blockchain::Transaction;
use exonum::encoding::serialize::FromHex;
use std::env;
use std::str::FromStr;

const SU_KEY: &str = "87fb7950f968e5121d13d5797d1faa37a13e77215e1e770af0c7456ca58a225e20c41714c92cf\
e7200f40b86fae30e6d6ae76aa2bf94526e0e4f54c9fd65efb9";

fn main() {
    let args: Vec<String> = env::args().collect();
    let tx: Box<Transaction> = if args.len() == 6 {
        let id = u64::from_str(&args[1]).unwrap();
        let first_name = &args[2];
        let last_name = &args[3];
        let info = &args[4];
        let pub_key = PublicKey::from_hex(&args[5]).unwrap();
        let su_key = SecretKey::from_hex(SU_KEY).unwrap();

        Box::new(TxCreateEmployee::new(
            id,
            &first_name,
            &last_name,
            &pub_key,
            &info,
            &su_key,
        ))
    } else {
        let target_pub_key = PublicKey::from_hex(&args[1]).unwrap();
        let first_name = &args[2];
        let last_name = &args[3];
        let info = &args[4];
        let su_key = SecretKey::from_hex(SU_KEY).unwrap();

        Box::new(TxUpdateEmployee::new(
            &target_pub_key,
            &first_name,
            &last_name,
            &target_pub_key,
            &info,
            &su_key,
        ))
    };

    println!("{}", tx.serialize_field().unwrap().get("signature").unwrap());
}