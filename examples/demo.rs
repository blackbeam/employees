extern crate exonum;
extern crate exonum_employees as employees;

use exonum::blockchain::{GenesisConfig, ValidatorKeys};
use exonum::node::{Node, NodeApiConfig, NodeConfig};
use exonum::storage::{RocksDB, RocksDBOptions};

use employees::service::EmployeesService;

fn node_config() -> NodeConfig {
    let (consensus_public_key, consensus_secret_key) = exonum::crypto::gen_keypair();
    let (service_public_key, service_secret_key) = exonum::crypto::gen_keypair();

    let validator_keys = ValidatorKeys {
        consensus_key: consensus_public_key,
        service_key: service_public_key,
    };
    let genesis = GenesisConfig::new(vec![validator_keys].into_iter());

    let api_address = "0.0.0.0:8000".parse().unwrap();
    let api_cfg = NodeApiConfig {
        public_api_address: Some(api_address),
        ..Default::default()
    };

    let peer_address = "0.0.0.0:2000".parse().unwrap();

    NodeConfig {
        genesis,
        listen_address: peer_address,
        external_address: None,
        network: Default::default(),
        peers: Default::default(),
        consensus_public_key,
        consensus_secret_key,
        service_public_key,
        service_secret_key,
        whitelist: Default::default(),
        api: api_cfg,
        mempool: Default::default(),
        services_configs: Default::default(),
    }
}

fn main() {
    exonum::helpers::init_logger().unwrap();

    println!("Create RocksDb database...");
    let db_path = ::std::env::current_dir().unwrap().join("db.rocksdb");
    let mut db_opts = RocksDBOptions::default();
    db_opts.create_if_missing(true);
    let db = RocksDB::open(db_path, &db_opts).unwrap();

    let node = Node::new(
        db,
        vec![Box::new(EmployeesService)],
        node_config(),
    );
    println!("Starting a single node...");
    println!("Blockchain is ready for transactions!");
    node.run().unwrap();
}