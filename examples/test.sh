#!/bin/bash
set -e

BASE_URL=http://127.0.0.1:8000/api/services/employees/v1
STATUS=0

function launch-server {
    cargo run --example demo &
    CTR=0
    MAXCTR=60
    while [[ ( -z `lsof -iTCP -sTCP:LISTEN -n -P 2>/dev/null |  awk '{ if ($9 == "*:8000") { print $2 } }'` ) && ( $CTR -lt $MAXCTR ) ]]; do
      sleep 1
      CTR=$(( $CTR + 1 ))
    done
    if [[ $CTR == $MAXCTR ]]; then
        echo "Failed to launch the server; aborting"
        exit 1
    fi
}

function kill-server {
    SERVER_PID=`lsof -iTCP -sTCP:LISTEN -n -P 2>/dev/null |  awk '{ if ($9 == "*:8000") { print $2 } }'`
    if [[ -n $SERVER_PID ]]; then
        kill -9 $SERVER_PID
    fi
}

function get-employee {
    RESP=`curl -H "Content-Type: application/json" -X GET $BASE_URL/employees/$1 2>/dev/null`
}

function create-employee {
    RESP=`curl -H "Content-Type: application/json" -X POST -d @$1 $BASE_URL/employees 2>/dev/null`
}

# Updates employee
#
# Arguments:
# - $1: employee's id
# - $2: transaction data
function update-employee {
    RESP=`curl -H "Content-Type: application/json" -X POST -d @$2 $BASE_URL/employees/update 2>/dev/null`
}

function check-transaction {
    if [[ `echo $RESP | jq .tx_hash` =~ ^\"$1 ]]; then
        echo "OK, got expected transaction hash $1"
    else
        echo "Unexpected response: $RESP"
        STATUS=1
    fi
}

# Checks a response to a read request.
#
# Arguments:
# - $1: employee's id
# - $2: employee's first_name
# - $3: employee's last_name
# - $4: employee's pub_key
# - $5: employee's info
# - $6: response JSON that encodes user's wallet information
function check-response {
    if  [[ ( `echo $6 | jq .id` == "\"$1\"" ) ]] && \
        [[ ( `echo $6 | jq .first_name` == "\"$2\"" ) ]] && \
        [[ ( `echo $6 | jq .last_name` == "\"$3\"" ) ]] && \
        [[ ( `echo $6 | jq .pub_key` == "\"$4\"" ) ]] && \
        [[ ( `echo $6 | jq .info` == "\"$5\"" ) ]]
    then
        echo "OK, got expected json for user with id $1"
    else
        # $RESP here is intentional; we want to output the entire incorrect response
        echo "Unexpected response: $RESP"
        STATUS=1
    fi
}

# Checks transaction status
#
# Arguments:
# - $1: transaction id
# - $2: transaction status type
# - $3: transaction status description
function check-tx-status {
    local RESP=`curl http://127.0.0.1:8000/api/explorer/v1/transactions/$1 2>/dev/null`
    if  [[ ( `echo $RESP | jq .status.type` == "\"$2\"" ) ]] && \
        [[ ( `echo $RESP | jq .status.description` == "\"$3\"" ) ]]
    then
        echo 'OK'
    else
        echo "Unexpected transaction status: $RESP"
        STATUS=1
    fi
}

kill-server
launch-server

echo
echo "Creating first employee"
create-employee create-employee-1.json
check-transaction 24e2177e
echo

echo "Creating second employee"
create-employee create-employee-2.json
check-transaction 9b6979c3
echo

echo "Waiting until transactions are committed..."
sleep 7
echo

echo "Check employees"
get-employee 1
check-response 1 Bob Gurish baf611d4929866aef785f1516f883f41116f99d53321416c19c54a067ed9d2f5 "foo" "$RESP"
get-employee 2
check-response 2 Alice Persky 6ce29b2d3ecadc434107ce52c287001c968a1b6eca3e5a1eb62a2419e2924b85 "" "$RESP"
echo

echo "Trying to create employee without first name"
create-employee create-employee-no-first-name.json
echo

echo "Waiting until transaction are committed..."
sleep 7
echo

echo "Check that transaction failed"
check-tx-status `echo $RESP | jq -r .tx_hash` error "No required first_name field"
echo

echo "Trying to create employee without last name"
create-employee create-employee-no-last-name.json
echo

echo "Waiting until transaction are committed..."
sleep 7
echo
echo "Check that transaction failed"
check-tx-status `echo $RESP | jq -r .tx_hash` "error" "No required last_name field"

echo
echo "Update employee using superuser's key"
update-employee 1 update-employee-1.json

echo
echo "Waiting until transaction are committed..."
sleep 7

echo
echo "Check employee"
get-employee 1
check-response 1 Bob Maddox baf611d4929866aef785f1516f883f41116f99d53321416c19c54a067ed9d2f5 "bar" "$RESP"

echo
echo "Update employee using employee's key"
update-employee 1 update-employee-2.json

echo
echo "Waiting until transaction are committed..."
sleep 7

echo
echo "Check employee"
get-employee 1
check-response 1 Bob Maddox c1898a132bce02eaf3f1dff62d4d14a1de0fbf67abfa26d915d0d40869794719 "baz" "$RESP"

kill-server
exit $STATUS
