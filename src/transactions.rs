use exonum::crypto::PublicKey;

use service::SERVICE_ID;

transactions! {
    pub(crate) EmployeesTransactions {
        const SERVICE_ID = SERVICE_ID;

        /// Transaction type for creating a new employee.
        struct TxCreateEmployee {
            /// Id of the employee
            id: u64,
            /// First name of the employee
            first_name: &str,
            /// Last name of the employee
            last_name: &str,
            /// Public key of the employee
            pub_key: &PublicKey,
            /// Optional additional information
            info: &str,
        }

        /// Transaction type for updating an existing employee.
        struct TxUpdateEmployee {
            /// Public key of an employee being updated
            target_pub_key: &PublicKey,
            /// First name of the employee
            first_name: &str,
            /// Last name of the employee
            last_name: &str,
            /// Public key of the employee
            pub_key: &PublicKey,
            /// Optional additional information
            info: &str,
        }
    }
}
