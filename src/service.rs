use exonum::blockchain::{Service, Transaction, ApiContext, TransactionSet};
use exonum::encoding::serialize::json::reexport::to_value;
use exonum::messages::RawTransaction;
use exonum::storage::{Fork, Snapshot};
use exonum::crypto::{Hash, PublicKey};
use exonum::encoding;
use exonum::encoding::serialize::json::reexport::Value;
use exonum::api::Api;
use iron::Handler;
use router::Router;
use std::io::{self, Read, ErrorKind};

use api::EmployeesApi;
use schema::EmployeeSchema;
use transactions::EmployeesTransactions;

/// Configuration file name
const CFG_FILE_NAME: &str = "config.toml";

/// External service config
#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    /// Superuser's public key
    pub su_pub_key: PublicKey,
}

/// This function will read configuration file.
fn read_config() -> io::Result<Config> {
    let path = ::std::env::current_dir().map(|dir| dir.join(CFG_FILE_NAME))?;
    let mut file = ::std::fs::File::open(path)?;
    let mut bytes = Vec::with_capacity(256);
    file.read_to_end(&mut bytes)?;
    ::toml::from_slice(&bytes[..])
        .map_err(|e| io::Error::new(ErrorKind::Other, e.to_string()))
}

/// Service ID for the `Service` trait.
pub const SERVICE_ID: u16 = 1;

// TODO: Document
/// Employees service.
///
/// # Public REST API
///
/// In all APIs, the request body (if applicable) and response are JSON-encoded.
///
/// ## Retrieve single employee by numeric id
///
/// GET `v1/employees/:id`
///
/// Returns information about an employee with the specified id (unsigned 64 bit integer).
/// If an employee with the specified id is not in the storage, returns a string
/// `"Employee not found"` with the HTTP 404 status.
pub struct EmployeesService;

impl Service for EmployeesService {
    fn service_name(&self) -> &'static str {
        "employees"
    }

    fn service_id(&self) -> u16 {
        SERVICE_ID
    }

    // Initializes superuser.
    // Requires superuser key in file `config.toml` in the cwd.
    fn initialize(&self, _fork: &mut Fork) -> Value {
        let config = read_config().expect("FATAL: Can't read configuration file");
        to_value(config).unwrap()
    }

    // Implement a method to deserialize transactions coming to the node.
    fn tx_from_raw(&self, raw: RawTransaction) -> Result<Box<Transaction>, encoding::Error> {
        let tx = EmployeesTransactions::tx_from_raw(raw)?;
        Ok(tx.into())
    }

    // Hashes for the service tables that will be included into the state hash.
    // To simplify things, we don't have [Merkelized tables][merkle] in the service storage
    // for now, so we return an empty vector.
    //
    // [merkle]: https://exonum.com/doc/architecture/storage/#merklized-indices
    fn state_hash(&self, snapshot: &Snapshot) -> Vec<Hash> {
        let schema = EmployeeSchema::new(snapshot);
        schema.state_hash()
    }

    // Create a REST `Handler` to process web requests to the node.
    fn public_api_handler(&self, ctx: &ApiContext) -> Option<Box<Handler>> {
        let mut router = Router::new();
        let api = EmployeesApi::new(ctx.node_channel().clone(), ctx.blockchain().clone());
        api.wire(&mut router);
        Some(Box::new(router))
    }
}
