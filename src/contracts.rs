use exonum::blockchain::{Schema, Transaction, ExecutionResult, Service};
use exonum::crypto::{CryptoHash, PublicKey};
use exonum::messages::Message;
use exonum::storage::{Fork, Snapshot};
use exonum::encoding::serialize::json::reexport::from_value;

use schema::{Employee, EmployeeSchema};
use service::{Config, EmployeesService};
use transactions::{TxCreateEmployee, TxUpdateEmployee};
use errors::Error;

const MAX_NAME_LEN: usize = 256;
const MAX_INFO_LEN: usize = 2048;

fn check_str_len_bounds(s: &str, left: usize, right: usize) -> bool {
    left <= s.len() && s.len() <= right
}

/// Will load superuser key from global service configuration
fn get_su_key<T>(view: T) -> PublicKey
    where T: AsRef<Snapshot>
{
    let schema = Schema::new(&view);
    let config = schema.actual_configuration();
    let config = config.services.get(EmployeesService.service_name());
    let config: Config = config
        .map(|config| from_value(config.clone()).unwrap())
        .unwrap();
    config.su_pub_key
}

impl Transaction for TxCreateEmployee {
    /// Verifies field limitations
    fn verify(&self) -> bool {
        check_str_len_bounds(self.first_name(), 1, MAX_NAME_LEN) &&
        check_str_len_bounds(self.last_name(), 1, MAX_NAME_LEN) &&
        check_str_len_bounds(self.info(), 0, MAX_INFO_LEN)
    }

    /// Checks, that transaction has all required fields.
    fn execute(&self, view: &mut Fork) -> ExecutionResult {
        // Only superuser is able to run this transaction.
        if !self.verify_signature(&get_su_key(&view)) {
            Err(Error::Forbidden)?
        }

        let mut schema = EmployeeSchema::new(view);

        // Check for possible pub_key conflict.
        if schema.key_exists(self.pub_key()) {
            Err(Error::EmployeeAlreadyExists)?
        }

        // Check for possible id conflict.
        if schema.id_exists(self.id()) {
            Err(Error::EmployeeAlreadyExists)?
        }

        let employee = Employee::new(
            self.id(),
            self.first_name(),
            self.last_name(),
            self.pub_key(),
            self.info(),
        );

        schema.employees_mut().put(self.pub_key(), employee);
        schema.set_id_idx_val(self.id(), self.pub_key().clone());
        schema.known_ids_mut().push(self.id());
        schema.employees_txs_mut(self.id()).push(self.hash());

        Ok(())
    }
}

impl Transaction for TxUpdateEmployee {
    /// Only super user or employee being updated is able to run this transaction.
    fn verify(&self) -> bool {
        check_str_len_bounds(self.first_name(), 1, MAX_NAME_LEN) &&
        check_str_len_bounds(self.last_name(), 1, MAX_NAME_LEN) &&
        check_str_len_bounds(self.info(), 0, MAX_INFO_LEN)
    }

    /// Checks, that transaction has all required fields.
    fn execute(&self, view: &mut Fork) -> ExecutionResult {
        // Only superuser or employee is able to run this transaction.
        if
            !self.verify_signature(&get_su_key(&view)) &&
            !self.verify_signature(self.target_pub_key())
        {
            Err(Error::Forbidden)?
        }

        let mut schema = EmployeeSchema::new(view);

        match schema.employee(&self.target_pub_key()) {
            Some(emp) => {
                let pub_key_changed = self.pub_key() != emp.pub_key();

                // Check for possible pub_key conflict.
                if pub_key_changed && schema.key_exists(&self.pub_key())
                {
                    Err(Error::EmployeeAlreadyExists)?
                }

                let employee = Employee::new(
                    emp.id(),
                    self.first_name(),
                    self.last_name(),
                    self.pub_key(),
                    self.info(),
                );

                // Remove old public key and update id index if pub_key changed
                if pub_key_changed {
                    schema.employees_mut().remove(&emp.pub_key());
                    schema.set_id_idx_val(emp.id(), self.pub_key().clone());
                }

                schema.employees_mut().put(&self.pub_key(), employee);
                schema.employees_txs_mut(emp.id()).push(self.hash());
            },
            None => Err(Error::EmployeeNotFound)?,
        }
        Ok(())
    }
}
