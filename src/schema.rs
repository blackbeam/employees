use byteorder::{LittleEndian as LE, ReadBytesExt, WriteBytesExt};
use exonum::storage::{Fork, ProofListIndex, ProofMapIndex, Snapshot};
use exonum::crypto::{Hash, PublicKey, HASH_SIZE};

const EMPLOYEES_NAME: &str = "org.employees";
const EMP_ID_IDX_NAME: &str = "org.emp_id_idx";
const KNOWN_IDS_NAME: &str = "org.known_ids";

/// Returns index name for employee's tx hashes list index
fn get_emp_tx_hashes_name(id: u64) -> String {
    format!("org.emp_txs_{}", id)
}

/// Employee's id as a key for ProofListMapIndex
pub type IdKey = [u8; HASH_SIZE];

/// Helper function to interpret u64 as IdKey.
/// Result will be [<eight bytes of u64 value in little endian>, <24 zero bytes>]
pub fn u64_to_id_key(n: u64) -> IdKey {
    let mut buf = IdKey::default();
    (&mut buf[..8]).write_u64::<LE>(n).unwrap(); // safe to unwrap, since u64 is 8 bytes long
    buf
}

/// Helper function to interpret IdKey as u64.
pub fn id_key_to_u64(key: IdKey) -> u64 {
    (&key[..8]).read_u64::<LE>().unwrap() // safe to unwrap, since u64 is 8 bytes long
}

encoding_struct! {
    /// Employee struct used to persist data within the service.
    struct Employee {
        /// Id of the employee
        id: u64,
        /// Fist name of the employee
        first_name: &str,
        /// Last name of the employee
        last_name: &str,
        /// Employee's public key
        pub_key: &PublicKey,
        /// Optional additional information
        info: &str,
    }
}

impl Employee {
    /// Returns a copy of the `Employee` where `id` field was set to `id`.
    pub fn set_id(self, id: u64) -> Self {
        Self::new(id, self.first_name(), self.last_name(), self.pub_key(), self.info())
    }

    /// Returns a copy of the `Employee` where `first_name` field was set to `first_name`.
    pub fn set_first_name(self, first_name: &str) -> Self {
        Self::new(self.id(), first_name, self.last_name(), self.pub_key(), self.info())
    }

    /// Returns a copy of the `Employee` where `last_name` field was set to `last_name`.
    pub fn set_last_name(self, last_name: &str) -> Self {
        Self::new(self.id(), self.first_name(), last_name, self.pub_key(), self.info())
    }

    /// Returns a copy of the `Employee` where `pub_key` field was set to `pub_key`.
    pub fn set_pub_key(self, pub_key: &PublicKey) -> Self {
        Self::new(self.id(), self.first_name(), self.last_name(), pub_key, self.info())
    }

    /// Returns a copy of the `Employee` where `info` field was set to `info`.
    pub fn set_info(self, info: &str) -> Self {
        Self::new(self.id(), self.first_name(), self.last_name(), self.pub_key(), info)
    }
}

/// Schema of the key-value storage used by the employee service.
pub struct EmployeeSchema<T> {
    view: T,
}

/// Declare the layout of data managed by the service.
impl<T: AsRef<Snapshot>> EmployeeSchema<T> {
    /// Creates a new schema instance.
    pub fn new(view: T) -> Self {
        Self { view }
    }

    /// Returns an immutable version of the employees table.
    pub fn employees(&self) -> ProofMapIndex<&Snapshot, PublicKey, Employee> {
        ProofMapIndex::new(EMPLOYEES_NAME, self.view.as_ref())
    }

    /// Returns an id to PublicKey index.
    pub fn emp_id_idx(&self) -> ProofMapIndex<&Snapshot, [u8; HASH_SIZE], PublicKey> {
        ProofMapIndex::new(EMP_ID_IDX_NAME, self.view.as_ref())
    }

    /// Returns view into transaction hashes associated with employees `id`.
    pub fn employees_txs(&self, id: u64) -> ProofListIndex<&Snapshot, Hash> {
        ProofListIndex::new(get_emp_tx_hashes_name(id), self.view.as_ref())
    }

    /// Return view into known employee's ids.
    pub fn known_ids(&self) -> ProofListIndex<&Snapshot, u64> {
        ProofListIndex::new(KNOWN_IDS_NAME, self.view.as_ref())
    }

    /// Gets a specific employee from the storage.
    pub fn employee(&self, pub_key: &PublicKey) -> Option<Employee> {
        self.employees().get(&pub_key)
    }

    /// Gets a specific employee by id.
    pub fn employee_by_id(&self, id: u64) -> Option<Employee> {
        self.emp_id_idx()
            .get(&u64_to_id_key(id))
            .and_then(|pub_key| self.employee(&pub_key))
    }

    /// Returns `true` if `pub_key` exists in the employees map.
    pub fn key_exists(&self, pub_key: &PublicKey) -> bool {
        self.employees().contains(&pub_key)
    }

    /// Returns `true` if `id` exists in the employee id index.
    pub fn id_exists(&self, id: u64) -> bool {
        self.emp_id_idx().contains(&u64_to_id_key(id))
    }

    /// Returns state hash values.
    pub fn state_hash(&self) -> Vec<Hash> {
        let mut state_hash = vec![
            self.employees().root_hash(),
            self.emp_id_idx().root_hash(),
            self.known_ids().root_hash(),
        ];

        let known_ids = self.known_ids();
        state_hash.extend(
            known_ids
                .iter()
                .map(|id| self.employees_txs(id).root_hash())
        );

        state_hash
    }
}

/// A mutable version of the schema.
impl<'a> EmployeeSchema<&'a mut Fork> {
    /// Returns a mutable version of the employees table.
    pub fn employees_mut(&mut self) -> ProofMapIndex<&mut Fork, PublicKey, Employee> {
        ProofMapIndex::new(EMPLOYEES_NAME, &mut self.view)
    }

    /// Returns a mutable version of the id to PublicKey index.
    fn emp_id_idx_mut(&mut self) -> ProofMapIndex<&mut Fork, [u8; HASH_SIZE], PublicKey> {
        ProofMapIndex::new(EMP_ID_IDX_NAME, &mut self.view)
    }

    /// Return a mutable version of known employee's ids list.
    pub fn known_ids_mut(&mut self) -> ProofListIndex<&mut Fork, u64> {
        ProofListIndex::new(KNOWN_IDS_NAME, &mut self.view)
    }

    /// Will map `id` to `pub_key` in employee's id index.
    pub fn set_id_idx_val(&mut self, id: u64, pub_key: PublicKey) {
        self.emp_id_idx_mut().put(&u64_to_id_key(id), pub_key);
    }

    /// Returns mutable view into transaction hashes associated with employees `id`.
    pub fn employees_txs_mut(&mut self, id: u64) -> ProofListIndex<&mut Fork, Hash> {
        ProofListIndex::new(get_emp_tx_hashes_name(id), &mut self.view)
    }
}
