use exonum::blockchain::ExecutionError;

/// Error codes emitted by transactions during execution.
#[derive(Debug, Fail)]
#[repr(u8)]
pub enum Error {
    /// No required first_name field.
    ///
    /// Can be emitted by `TxCreateEmployee` and `TxUpdateEmployee`.
    #[fail(display = "No required first_name field")]
    NoRequiredFirstNameField = 0,

    /// No required last_name field.
    ///
    /// Can be emitted by `TxCreateEmployee` and `TxUpdateEmployee`.
    #[fail(display = "No required last_name field")]
    NoRequiredLastNameField = 1,

    /// Employee already exists.
    ///
    /// Can be emitted by `TxCreateEmployee`.
    #[fail(display = "Employee already exists")]
    EmployeeAlreadyExists = 2,

    /// Employee not found.
    ///
    /// Can be emitted by `TxUpdateEmployee`.
    #[fail(display = "Employee not found")]
    EmployeeNotFound = 3,

    /// Invalid signature.
    ///
    /// Can be emitted by `TxCreateEmployee` and `TxUpdateEmployee`.
    #[fail(display = "Forbidden")]
    Forbidden = 4,
}

impl From<Error> for ExecutionError {
    fn from(value: Error) -> ExecutionError {
        let description = format!("{}", value);
        ExecutionError::with_description(value as u8, description)
    }
}
