use exonum::api::{Api, ApiError};
use exonum::blockchain::{Blockchain, Transaction};
use exonum::explorer::BlockchainExplorer;
use exonum::crypto::{Hash, PublicKey};
use exonum::encoding::serialize::FromHex;
use exonum::node::{TransactionSend, ApiSender};
use iron::prelude::*;
use iron::status::Status;
use iron::headers::ContentType;
use iron::modifiers::Header;
use router::Router;
use std::str::FromStr;

use bodyparser;
use serde_json;
use schema::EmployeeSchema;
use transactions::EmployeesTransactions;

/// Container for the service API.
#[derive(Clone)]
pub struct EmployeesApi {
    channel: ApiSender,
    blockchain: Blockchain,
}

impl EmployeesApi {
    /// Constructor.
    pub fn new(channel: ApiSender, blockchain: Blockchain) -> EmployeesApi {
        EmployeesApi { channel, blockchain }
    }

    fn get_employee_by_key(&self, req: &mut Request) -> IronResult<Response> {
        let path = req.url.path();
        let pub_key_param = path.last().unwrap();
        let pub_key = PublicKey::from_hex(pub_key_param)
            .map_err(|e| {
                IronError::new(e, (
                    Status::BadRequest,
                    Header(ContentType::json()),
                    "\"Invalid request param: `pub_key`\"",
                ))
            })?;

        let snapshot = self.blockchain.snapshot();
        let schema = EmployeeSchema::new(snapshot);

        if let Some(employee) = schema.employee(&pub_key) {
            self.ok_response(&serde_json::to_value(employee).unwrap())
        } else {
            self.not_found_response(&serde_json::to_value("Employee not found").unwrap())
        }
    }

    fn get_employee_by_id(&self, req: &mut Request) -> IronResult<Response> {
        let path = req.url.path();
        let id_param = path.last().unwrap();
        let id = u64::from_str(id_param)
            .map_err(|e| {
                IronError::new(e, (
                    Status::BadRequest,
                    Header(ContentType::json()),
                    "\"Invalid request param: `id`\"",
                ))
            })?;

        let snapshot = self.blockchain.snapshot();
        let schema = EmployeeSchema::new(snapshot);

        if let Some(employee) = schema.employee_by_id(id) {
            self.ok_response(&serde_json::to_value(employee).unwrap())
        } else {
            self.not_found_response(&serde_json::to_value("Employee not found").unwrap())
        }
    }

    fn get_employees_blocks(&self, req: &mut Request) -> IronResult<Response> {
        let path = req.url.path();
        let id_param = path.last().unwrap();
        let id = u64::from_str(id_param)
            .map_err(|e| {
                IronError::new(e, (
                    Status::BadRequest,
                    Header(ContentType::json()),
                    "\"Invalid request param: `id`\"",
                ))
            })?;

        let snapshot = self.blockchain.snapshot();
        let schema = EmployeeSchema::new(snapshot);
        let explorer = BlockchainExplorer::new(&self.blockchain);

        let mut heights = Vec::new();
        let employees_txs = schema.employees_txs(id);
        for tx_hash in employees_txs.iter() {
            let tx_info = explorer.tx_info(&tx_hash)?;
            if let Some(tx_info) = tx_info {
                heights.push(tx_info.location.block_height().0)
            }
        }
        self.ok_response(&serde_json::to_value(heights).unwrap())
    }

    fn create_employee(&self, req: &mut Request) -> IronResult<Response> {
        match req.get::<bodyparser::Struct<EmployeesTransactions>>() {
            Ok(Some(transaction)) => {
                let transaction: Box<Transaction> = transaction.into();
                let tx_hash = transaction.hash();
                self.channel.send(transaction).map_err(ApiError::from)?;
                let json = TransactionResponse { tx_hash };
                self.ok_response(&serde_json::to_value(&json).unwrap())
            }
            Ok(None) => Err(ApiError::BadRequest("Empty request body".into()))?,
            Err(e) => {
                println!("{:?}", e);
                Err(ApiError::BadRequest(e.to_string()))?
            },
        }
    }

    fn update_employee(&self, req: &mut Request) -> IronResult<Response> {
        match req.get::<bodyparser::Struct<EmployeesTransactions>>() {
            Ok(Some(transaction)) => {
                let transaction: Box<Transaction> = transaction.into();
                let tx_hash = transaction.hash();
                self.channel.send(transaction).map_err(ApiError::from)?;
                let json = TransactionResponse { tx_hash };
                self.ok_response(&serde_json::to_value(&json).unwrap())
            },
            Ok(None) => Err(ApiError::BadRequest("Empty request body".into()))?,
            Err(e) => {
                println!("{:?}", e);
                Err(ApiError::BadRequest(e.to_string()))?
            }
        }
    }
}

impl Api for EmployeesApi {
    fn wire(&self, router: &mut Router) {
        let get_employee_by_id = {
            let this = self.clone();
            move |req: &mut Request| this.get_employee_by_id(req)
        };
        let get_employee_by_key = {
            let this = self.clone();
            move |req: &mut Request| this.get_employee_by_key(req)
        };
        let get_employees_blocks = {
            let this = self.clone();
            move |req: &mut Request| this.get_employees_blocks(req)
        };
        let create_employee = {
            let this = self.clone();
            move |req: &mut Request| this.create_employee(req)
        };
        let update_employee = {
            let this = self.clone();
            move |req: &mut Request| this.update_employee(req)
        };

        router.get("/v1/keys/:pub_key", get_employee_by_key, "get_employee_by_key");
        router.get("/v1/ids/:id", get_employee_by_id, "get_employee_by_id");
        router.get("/v1/blocks/:id", get_employees_blocks, "get_employees_blocks");
        router.post("/v1/update", update_employee, "update_employee");
        router.post("/v1/create", create_employee, "create_employee");
    }
}

/// The structure returned by the REST API.
#[derive(Serialize, Deserialize)]
pub struct TransactionResponse {
    /// Hash of the transaction.
    pub tx_hash: Hash,
}
